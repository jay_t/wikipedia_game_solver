# Wikipedia Game Solver

This script tried to find a path from one Wikipedia page to another. It uses asynchronous functionality from the [trio](https://trio.readthedocs.io/) and [asks](https://asks.readthedocs.io/) libraries to fetch multiple pages at once. 