from sys import argv
import asks
import trio
import cursive_re as cr
import queue
from typing import List, Any
from collections import namedtuple
from datetime import datetime
from bs4 import BeautifulSoup


def get_links_from_page(page: asks.response_objects.Response) -> List[str]:
    """This function takes a page and uses a BeautifulSoup4 to extract all the links"""

    soup = BeautifulSoup(page.text, features="lxml")
    body = soup.find(id="mw-content-text")

    cursive_regex = cr.alternative(cr.text('https://en.wikipedia.org'), cr.text('')) \
        + cr.text('/wiki/') + cr.one_or_more(cr.none_of('#:')) + cr.end_of_line()

    r = cr.compile(cursive_regex)

    def get_href(link):
        try:
            return link['href']
        except KeyError:
            return ''

    links = [
        link['href'] for link in body.find_all('a')
        if r.match(get_href(link))
    ]

    return links


def get_page_name(url: str) -> str:
    """This function will take a str and extract the name of the wikipedia page from the URL if it finds one"""

    cursive_regex = cr.zero_or_more(cr.anything()) + cr.text('/wiki/') + cr.group(cr.zero_or_more(cr.anything()))
    regex = cr.compile(cursive_regex)
    m = regex.match(url)
    if m:
        return m.group(1)
    else:
        return url


Page = namedtuple("Page", "url parent")


async def run_page(page: Page, q: queue.SimpleQueue, session, visited_pages: set) -> None:
    """Async function to fetch a page content"""
    print(f"### Fetching page {page.url}")
    try:
        fetched_page = await session.get(page.url)
    except (asks.errors.BadHttpResponse, trio.BrokenResourceError) as e:
        print(f"Fetching failed for page {page.url} with error {e}. Re-adding to queue")
        visited_pages.remove(page.url)  # The visit failed
        q.put(page)  # We need to retry this page later
        await trio.sleep(15)  # Sleep this thread without completing it for 15 seconds to back off for a bit
        return
    links = get_links_from_page(fetched_page)
    for l in links:
        q.put(Page(f"https://en.wikipedia.org/wiki/{get_page_name(l)}", parent=page))


async def run_search(start: str, end: str, thread_limit: int = 50) -> None:
    """Async parent function for Trio"""

    # Set up some variables
    end_name = get_page_name(end)
    start_time = datetime.now()
    visited_pages = set()  # Take note of pages we have already looked at
    page_queue = queue.SimpleQueue()  # A FIFO queue to put pages in
    page_queue.put(Page(start, None))  # Put in the start page to get us going

    async with trio.open_nursery() as nursery:
        # Create a shared session for requests
        print(f"Running with thread limit of {thread_limit} threads.")
        session = asks.Session('https://en.wikipedia.org', connections=thread_limit)
        while True:
            while True:  # Do-while loop
                try:
                    page: Page = page_queue.get(block=False)  # Grab the next page out of the queue
                except queue.Empty:  # But it might be empty if we are still waiting on requests to finish
                    await trio.sleep(0.1)  # In that case, sleep for a bit, then try again
                    continue
                if page.url in visited_pages:
                    print(f"--- Skipping page {page.url}. Already visited.")
                else:
                    break  # We have found a page that hasn't yet been visited, so now go fetch it
            visited_pages.add(page.url)  # Mark it as visited, so we don't try it again

            # Check if we are at the right page
            if get_page_name(page.url) == end_name:
                nursery.cancel_scope.cancel()  # Cancel all running requests
                break  # and exit the while loop.

            # Wait till there are less than the thread limit of tasks running, so as not to crash Trio
            # with ValueError: too many file descriptors in select()
            # If you make too many connections at the same time Wikipedia will drop some of the connections
            while len(nursery.child_tasks) > thread_limit:
                await trio.sleep(0.1)

            # Now that there are less that 50, fetch this page
            nursery.start_soon(run_page, page, page_queue, session, visited_pages)

    print(f"Hooray, we are at the right URL! {page.url}")
    print("To get there we went:")

    def get_parent(page: Page) -> Any:
        """This functions just un-stacks the visit chain"""
        if page.parent is None:
            return page.url
        else:
            parents = get_parent(page.parent)
            if isinstance(parents, tuple):
                return (*parents, page.url)
            return (parents, page.url)

    reversed_stack = get_parent(page)
    print(" -> ".join(reversed_stack))
    end_time = datetime.now()
    print(f'It took {end_time - start_time} to find.')
    return


def main():
    asks.init('trio')
    start = "https://en.wikipedia.org/wiki/{}".format(input('What is the start page?\n'))
    end = "https://en.wikipedia.org/wiki/{}".format(input('What is the end page?\n'))
    requested_thread_limit = input('What thread limit do you want?\n')
    if requested_thread_limit == '':
        requested_thread_limit = 0
    tl = int(requested_thread_limit) if int(requested_thread_limit) < 100 and int(requested_thread_limit) > 0 else 50
    trio.run(run_search, start, end, tl)


if __name__ == '__main__':
    if len(argv) == 1:
        main()
